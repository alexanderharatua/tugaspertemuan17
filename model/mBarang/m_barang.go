package mBarang



import (
	"database/sql"
	"time"
)

type Barang struct {
	Id          int64
	Nama        string // `json:"nim"`
	Jenis       string
	Kode  		string
	TglMasuk time.Time
	TglKeluar time.Time
	Tglexpired time.Time
}

const dateFormat = `2006-01-02 15:04:05`

func SelectAll(db *sql.DB) (barang []Barang, err error) {
	rows, err := db.Query(`SELECT * FROM barang ORDER BY id DESC`)
	barang = []Barang{}
	if err != nil {
		return barang, err
	}
	defer rows.Close()
	for rows.Next() {
		s := Barang{}
		tglMasuk, tglKeluar, tglexpired := ``, ``,``
		err = rows.Scan(
			&s.Id,
			&s.Nama,
			&s.Jenis,
			&s.Kode,
			&tglMasuk,
			&tglKeluar,
			&tglexpired)
		if err != nil {
			return
		}
		s.TglMasuk, _ = time.Parse(dateFormat, tglMasuk)
		s.TglKeluar, _ = time.Parse(dateFormat, tglKeluar)
		barang = append(barang, s)
	}
	return
}

func Insert(db *sql.DB, m *Barang) (err error) {
	now := time.Now()
	res, err := db.Exec(`INSERT INTO barang(nama,jenis,kode,tgl_masuk,tgl_keluar,tgl_expired)
VALUES(?,?,?,?,?,?)`,
		m.Nama,
		m.Jenis,
		m.Kode,
		now,
		now,
		now)
	if err != nil {
		return err
	}
	m.Id, err = res.LastInsertId()
	if err == nil {
		m.TglMasuk = now
		m.TglKeluar = now
	}
	return err
}

