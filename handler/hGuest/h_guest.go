package hGuest

import (
	"TUGASPERTEMUAN17/handler"
	"TUGASPERTEMUAN17/model/mBarang"
	"encoding/json"
	"net/http"
)

func ApotikList(ctx *handler.Ctx) {
	barang, err := mBarang.SelectAll(ctx.Db)
	if ctx.IsError(err) {
		return
	}
	ctx.End(barang)
}

func ApotikCreate(ctx *handler.Ctx) {
	if ctx.Request.Method == `GET` {
		http.ServeFile(ctx, ctx.Request, ctx.ViewsDir+`guest/student_create.html`)
		return
	}
	m := mBarang.Barang{}
	err := json.NewDecoder(ctx.Request.Body).Decode(&m)
	if ctx.IsError(err) {
		return
	}
	err = mBarang.Insert(ctx.Db, &m)
	if ctx.IsError(err) {
		return
	}
	ctx.End(m)
}
