package main

import (
	"TUGASPERTEMUAN17/handler"
	"TUGASPERTEMUAN17/handler/hGuest"
)

const PORT = `:8080`

func main() {
	server := handler.InitServer(`views/`)
	server.Handle(`/guest/barang/list`, hGuest.ApotikList)
	server.Handle(`/guest/barang/create`, hGuest.ApotikCreate)
	server.Listen(PORT)
}
